const withMT = require("@material-tailwind/react/utils/withMT");
 
module.exports = withMT({
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        dark: '#1f1f1f',
        light: '#f5f5f5',
        defaultColor: '#E6EBE9' 
      },
    },
  },
  plugins: [
      function ({ addUtilities }) {
        const newUtilities = {
          ".no-scrollbar::-webkit-scrollbar": {
            display: "none",
          },
          ".no-scrollbar": {
            "-ms-overflow-style": "none",
            "scrollbar-width": "none",
          },
        };
        addUtilities(newUtilities);
      },
      require('tailwind-scrollbar'),
    ],
});