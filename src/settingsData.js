const SettingsData = [
		{
			id: '1',
			name: 'title',
			description: 'This is a sample description!'
		},
		{
			id: '2',
			name: 'title 2',
			description: 'This is a sample description!'
		},
		{
			id: '3',
			name: 'title 3',
			description: 'This is a sample description!'
		},
		{
			id: '4',
			name: 'title 4',
			description: 'This is a sample description!'
		},
		{
			id: '5',
			name: 'title 5',
			description: 'This is a sample description! This is a sample description!'
		},
		{
			id: '6',
			name: 'title',
			description: 'This is a sample description!'
		},
		{
			id: '7',
			name: 'title 2',
			description: 'This is a sample description!'
		},
		{
			id: '8',
			name: 'title 3',
			description: 'This is a sample description!'
		},
		{
			id: '9',
			name: 'title 4',
			description: 'This is a sample description!'
		},
		{
			id: '10',
			name: 'title 5',
			description: 'This is a sample description! This is a sample description!'
		}
]

export default SettingsData; 