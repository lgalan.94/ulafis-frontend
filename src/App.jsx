import './App.css';
import { UserProvider } from './UserContext.js';
import { Routes, Route, useLocation } from 'react-router-dom';
import { Landing, Home, UpdateItem, Categories, UpdateCategory, Notifications, Inventory } from './pages';
import { AddItem } from './pages/content';
import Login from './auth/Login';
import Logout from './auth/Logout';
import { Dashboard, Users, Reports, Settings } from './pages/admin';
import { ViewSetting } from './pages/admin/view';
import { AddSettings } from './pages/admin/content';
import { useState, useEffect } from 'react';

function App() {

  const [user, setUser] = useState({
    id: null,
    email: null,
    userRole: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  const Inaccessible = () => {
   return < Navigate to = '/not-found' />
  }


  /*fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/details`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(result => result.json())
  .then(data=> {
    setUser({
      id: data._id,
      email: data.email,
      userRole: data.userRole
    })
  })*/
  
  const location = useLocation();

  return (

    <UserProvider value = {{ user, setUser, unsetUser }} >
      <Routes key={location.pathname} location={location}> 
        <Route path="/ulafis/login" element={<Login />} />
        <Route path="/ulafis/logout" element={<Logout />} />

        <Route index element={<Landing />}  />
        
        <Route path="/ulafis/home" element={<Home />} />
        <Route path="/ulafis/inventory" element={<Inventory />} />
        <Route path="/ulafis/inventory/add-item" element={<AddItem />} />
        <Route path="/ulafis/notifications" element={<Notifications />} />
        <Route path="/ulafis/update/:itemId" element={<UpdateItem />} />
        <Route path="/ulafis/categories" element={<Categories />} />
        <Route path="/ulafis/categories/update/:categoryId" element={<UpdateCategory />} />

        <Route path="/ulafis/admin" element={<Dashboard />} />
        <Route path="/ulafis/admin/manage-users" element={<Users />} />
        <Route path="/ulafis/admin/reports" element={<Reports />} />
        <Route path="/ulafis/admin/settings" element={<Settings />} />
        <Route path="/ulafis/admin/settings/add" element={<AddSettings />} />
        <Route path="/ulafis/admin/settings/view/:settingsId" element={<ViewSetting />} />
      </Routes>
    </UserProvider>

    
  )
}

export default App
