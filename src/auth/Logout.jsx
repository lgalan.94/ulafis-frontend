import { useNavigate } from 'react-router-dom';
import { useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';

export default function Logout() {
  const { unsetUser, setUser, user } = useContext(UserContext);
  let navigate = useNavigate();

  useEffect(() => {
    unsetUser();
    setUser({
      id: null,
      email: null,
      userRole: null
    });
  }, [user]);

  return (
      navigate('/')
  );
}