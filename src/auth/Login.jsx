import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { ImSpinner2 } from "react-icons/im";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Card, CardHeader, CardBody, CardFooter, Typography, Input, Button} from "@material-tailwind/react";
import { IoMdArrowBack } from "react-icons/io";
  
const Login = () => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isClicked, setIsClicked] = useState(false);

  const navigate = useNavigate();
  const { user, setUser } = useContext(UserContext);

  const BackToHome = () => {
    return navigate('/');
  }

  const UserDetails = (token) => {
    fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/details`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(result => result.json())
      .then(data => {
        setUser({
          id: data._id,
          userRole: data.userRole
        });

        if (data.userRole === 0) {
          setTimeout(() => navigate('/ulafis/home'), 1000);
          setTimeout(() => setIsClicked(false), 1000);
          
        } else {
          setTimeout(() => navigate('/'), 900);
          setTimeout(() => setIsClicked(false), 1000);
        }
      });
  }; 

  const handleLogin = (e) => {
    e.preventDefault();
    setIsClicked(true);
    fetch(`${import.meta.env.VITE_API_URL}/ulafis/user/login`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(result => result.json())
      .then(data => {
        if (data === false) {
          toast.error('Incorrect email or password!');
          setTimeout(() => 1500);
          setTimeout(() => setIsClicked(false), 1000);
        } else {
          localStorage.setItem('token', data.auth);
          UserDetails(data.auth);
          toast.success("Login Successful!");
          setTimeout(() => setIsClicked(false), 1000);
        }
      });
  };

  return (

   <>
     <div className="flex relative flex-col h-screen justify-center items-center bg-defaultColor">
      
       <Card className="w-96">
         <CardHeader
           variant="gradient"
           color="gray"
           className="mb-4 grid h-28 place-items-center"
         >
           <Typography 
             variant="h3" 
             color="white"
             className="uppercase"
             >
             Login
             
           </Typography>
         </CardHeader>
         <CardBody>
         <form className="flex flex-col gap-4" onSubmit={handleLogin}>
           <Input 
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              label="Email"
              size="lg" 
            />
           <Input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)} 
              label="Password"
              size="lg"
            />

            <div className="pt-0 flex flex-row gap-1">
            <Button onClick={BackToHome} variant="text" className="hover:scale-105 outline outline-1 outline-dark/25" >
              <IoMdArrowBack />
            </Button>

              {/*{
                isClicked ? (
                    <>
                    <Button className="flex flex-row items-center capitalize justify-center" disabled fullWidth>
                      <ImSpinner2 className="animate-spin mr-1 w-4 h-4" /> Logging in...
                    </Button>
                    </>
                  ) : (
                    
                    <Button type="submit" className="capitalize" fullWidth>
                      login
                    </Button>
                  )
              }*/}

              <Button onClick={() => navigate('/ulafis/home') }  className="hover:scale-105 outline outline-1 outline-dark/25" fullWidth>
                Lab Manager
              </Button>
              <Button onClick={() => navigate('/ulafis/admin')}  className="hover:scale-105 outline outline-1 outline-dark/25" fullWidth>
                Admin
              </Button>

            </div>
          </form>
         </CardBody>
       </Card>
     </div>

     <ToastContainer position="top-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
      
   </>
    
  );
}

export default Login