import { DefaultNavbar, DefaultSidebar, Layout, ItemCard, CustomButton, CustomAlert, CustomTitle } from '../components';
import { useParams, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { IoArrowBack } from "react-icons/io5";
import { ImSpinner2 } from "react-icons/im";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Tooltip } from '@material-tailwind/react';
import { LuSaveAll } from "react-icons/lu";
import { RiRefreshFill } from "react-icons/ri";

const UpdateItem = () => {

let navigate = useNavigate();
let { itemId } = useParams();
const [name, setItemName] = useState('');
const [description, setItemDescription] = useState('');
const [quantity, setItemQuantity] = useState('');
const [location, setItemLocation] = useState('');
const [category, setItemCategory] = useState('');
const [image_url, setItemImageUrl] = useState('');
const [low_stock_threshold, set_low_stock_threshold] = useState(0);
const [isDisabled, setIsDisabled] = useState(true);
const [isClicked, setIsClicked] = useState(false);
const [loading, setLoading] = useState(true);

const [nameAlert, setNameAlert] = useState(false);
const [descriptionAlert, setDescriptionAlert] = useState(false);
const [quantityAlert, setQuantityAlert] = useState(false);
const [lowStockAlert, setLowStockAlert] = useState(false);
const [locationAlert, setLocationAlert] = useState(false);
const [categoryAlert, setCategoryAlert] = useState(false);
const [imageUrlAlert, setImageUrlAlert] = useState(false);
const [thresholdAlert, setThresholdAlert] = useState(true);

useEffect(() => {
  if (parseInt(low_stock_threshold) >= parseInt(quantity)) {
    setThresholdAlert(true);
    setIsDisabled(true);
  } else {
    setThresholdAlert(false);
    setIsDisabled(false);
  }
}, [low_stock_threshold, quantity]);

useEffect(() => {
		fetch(`${import.meta.env.VITE_API_URL}/ulafis/items/${itemId}`)
		.then(response => response.json())
		.then(data => {
				setItemName(data.name);
				setItemDescription(data.description);
				setItemQuantity(data.quantity);
				setItemLocation(data.location);
				setItemCategory(data.category);
				setItemImageUrl(data.image_url);
				set_low_stock_threshold(data.low_stock_threshold);
				setLoading(false);
		})
}, [itemId])

const handleBack = () => {
		navigate('/ulafis/inventory')
}

const refresh = () => {
		setLoading(true);
		setTimeout(() => setLoading(false), 1000)
}	

const handleUpdate = (e) => {
		e.preventDefault();
		setIsClicked(true);
		setIsDisabled(true);
		fetch(`${import.meta.env.VITE_API_URL}/ulafis/items/${itemId}`, {
				method: "PATCH",
				headers: {
						'Content-type': 'application/json'
				},
				body: JSON.stringify({
						name,
						description,
						quantity,
						location,
						category,
						image_url
				})
		})
		.then(response => response.json())
		.then(data => {
					if (data === true) {
								toast.success(`${name} Successfully Updated! `);
								setTimeout(() => navigate('/ulafis/inventory'), 1000 )
					}
		})

}
const handleNameChange = (e) => {
	e.preventDefault();
	setItemName(e.target.value);
	setIsDisabled(false);
}
const handleDescriptionChange = (e) => {
	e.preventDefault();
	setItemDescription(e.target.value);
	setIsDisabled(false);
}
const handleQuantityChange = (e) => {
	e.preventDefault();
	setItemQuantity(e.target.value);
	setIsDisabled(false);
}
const handleLocationChange = (e) => {
	e.preventDefault();
	setItemLocation(e.target.value);
	setIsDisabled(false);
}
const handleCategoryChange = (e) => {
	setItemCategory(e);
	setIsDisabled(false);
}
const handleImageUrlChange = (e) => {
	e.preventDefault();
	setItemImageUrl(e.target.value);
	setIsDisabled(false);
}
const handleLowStockThreshold = (e) => {
	e.preventDefault();
	set_low_stock_threshold(e.target.value);
	setIsDisabled(false);
}

	return (
					<>
							<CustomTitle title="U-LAFIS | Update Item" />
							<DefaultNavbar userRole="lab manager" />
							<div className="flex flex-row">
									<DefaultSidebar />
									<Layout
										layout="w-full"
										component1={
													<CustomButton
															label="back"
															variant="text"
															icon={<IoArrowBack className="h-3 w-3" />}
															btnClass="lowercase"
															handleClick={handleBack}
													/>
										}
										component2={
													<Tooltip 
															placement="left"
															content="refresh"
													  className={`bg-cyan-600 text-xs`}
													>
															<button onClick={refresh} className={`flex self-center hover:scale-105 hover:shadow-lg hover:z-10 rounded-full text-cyan-600`}> 
															    <RiRefreshFill className='w-8 h-8' /> 
															</button>
													</Tooltip>
										}
									>
										{
												!loading ? (
														<>
																<ItemCard
																	imageUrl={image_url}
																	name={name.toUpperCase()}
																	description={description}
																	quantity={quantity}
																	location={location}
																	category={category}
																	categoryLabel={category}
																	image_url={image_url}
																	lowStockThreshold={low_stock_threshold}

																	handleNameChange={handleNameChange}
																	handleDescriptionChange={handleDescriptionChange}
																	handleQuantityChange={handleQuantityChange}
																	handleLocationChange={handleLocationChange}
																	handleCategoryChange={handleCategoryChange}
																	handleImageUrlChange={handleImageUrlChange}
																	handleLowStockThreshold={handleLowStockThreshold}

																	displayAlert={
																				<>
																					<CustomAlert
																							openAlert={nameAlert}
																							alertLabel="Name alert!"
																					/>
																					<CustomAlert
																							openAlert={descriptionAlert}
																							alertLabel="Description alert!"
																					/>
																					<CustomAlert
																							openAlert={quantityAlert}
																							alertLabel="Quantity alert!"
																					/>
																					<CustomAlert
																							openAlert={lowStockAlert}
																							alertLabel="Low Stock alert!"
																					/>
																					<CustomAlert
																							openAlert={locationAlert}
																							alertLabel="Location alert!"
																					/>
																					<CustomAlert
																							openAlert={categoryAlert}
																							alertLabel="Category alert!"
																					/>
																					<CustomAlert
																							openAlert={imageUrlAlert}
																							alertLabel="Image alert!"
																					/>
																					<CustomAlert
																							openAlert={thresholdAlert}
																							alertLabel="Low Stock Threshold value must be lower than the number of quantity! "
																					/>
																				</>
																	}

																/>

																<div className="mt-8 flex flex-row gap-1 justify-center mx-auto">
																    
																    <CustomButton
																      label="cancel"
																      handleClick={handleBack}
																      btnClass="rounded px-3 py-2 hover:scale-105"
																    />
																    {
																      !isClicked ? (
																         <CustomButton
																           label="update"
																           icon={<LuSaveAll />}
																           handleClick={handleUpdate}
																           btnClass="rounded px-3 py-2 hover:scale-105"
																           color="green"
																           isDisabled={isDisabled}
																         />
																       ) : (
																         <CustomButton
																           label="Updating..."
																           btnClass="rounded px-3 py-2 hover:scale-105"
																           color="green"
																           icon={<ImSpinner2 className="animate-spin" />}
																           isDisabled={true}
																         />
																       )
																    }
																</div>
																</>
													) : (
															<ImSpinner2 className="w-12 h-12 min-h-[67vh] mx-auto animate-spin" />
													)
										}
									</Layout>
							</div>

					</>
	)
}

export default UpdateItem