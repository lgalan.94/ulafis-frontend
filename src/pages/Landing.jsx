import { DefaultNavbar, Layout, Footer, Banner, About, CustomTitle, Loading } from '../components';
import { Button } from '@material-tailwind/react';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const Landing = () => {

	let navigate = useNavigate();
	const [loading, setLoading] = useState(true);
	let [systemName, setSystemName] = useState();

	useEffect(() => {
	    fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings`)
	      .then((result) => result.json())
	      .then((data) => {
	        const system = data.find((setting) => setting.key === 'SYSTEM NAME'); 
	        if (system) {
	          setSystemName(system.value);
	        }
	        setLoading(false);
	      });
	  }, []);

	const scrollToSection = (sectionId) => {
	  const element = document.getElementById(sectionId);
	  element.scrollIntoView({ behavior: "smooth" });
	};

	const ScrollToHome = () => {
	  scrollToSection("home")
	}

	const ScrollToAbout = () => {
	  scrollToSection("about")
	}

	const CustomLink = ({ title, scrollTo }) => {

	  return (
	    <button onClick={scrollTo} className="relative text-[10px] tracking-widest uppercase font-semibold group hover:text-dark" >
	      {title}

	      <span className={`h-[2px] inline-block w-0 lg:bg-dark bg-none opacity-50 absolute left-0 -bottom-2 group-hover:w-full transition-[width] ease duration-300`}>
	        &nbsp;
	      </span>
	    </button>
	  );
	};

	const navList = (
	  <ul className="flex gap-6">
	    <CustomLink title="Home" scrollTo={ScrollToHome} />
	    <CustomLink title="About" scrollTo={ScrollToAbout} />
	  </ul>
	);

	return (
		<>
			<CustomTitle title="U-LAFIS | Landing Page " />
			
			{
				!loading ? (
							<>
									<DefaultNavbar />
									<Layout 
										chip="hidden"
										count="0"
										card="!bg-defaultColor shadow-none"
										layout="px-10"
										layoutHeader="hidden"
										layoutHeight=""
									>
											<div className="sticky top-0 z-10 w-full ">
											   <div className="flex justify-end">{navList}</div>
											</div>
											<div id="home">
													<Banner 
														headline="Streamline your lab workflow with"
														title={systemName}
														subheading="Increase efficiency, reduce errors, and save time with our powerful U-LAFIS"
													/>
											</div>
											<div id="about">
													<About 
														title="About U-LAFIS"
														image="https://media.istockphoto.com/id/1346675635/photo/modern-medical-research-laboratory-portrait-of-latin-and-black-young-scientists-using.jpg?s=612x612&w=0&k=20&c=9PiRizOrO04C6TSyMhqv7NUDabjhbrrnL3LVLuArGJU="
														subtitle="Revolutionizing Laboratory Management"
														description="U-LIMS is a revolutionary Laboratory and Inventory Management System (LIMS) designed to empower laboratories of all sizes to improve efficiency, accuracy, and compliance. We understand the challenges that laboratories face in managing samples, inventory, and data, and we are committed to providing a comprehensive and user-friendly solution that addresses these needs."
													/>
											</div>
									</Layout>
									<Footer />
							</>
					) : (
							<Loading />
					)
			}

		</>
	)
}

export default Landing