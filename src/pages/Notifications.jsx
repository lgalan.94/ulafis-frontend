import { DefaultNavbar, DefaultSidebar, Layout, CustomTitle } from '../components';
import { Typography } from '@material-tailwind/react';

const Notifications = () => {
		return (
						<>
								<CustomTitle title="U-LAFIS | Notifications" />
								<DefaultNavbar userRole="lab manager" />
								<div className="flex flex-row">
										<DefaultSidebar />
															<Layout
																	layout="w-full"
																	
															>
																<Typography variant="h1">Notifications Page</Typography>
															</Layout>
								</div>

						</>
			)
}

export default Notifications