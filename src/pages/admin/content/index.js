import ManageUsers from './ManageUsers'
import ManageSettings from './ManageSettings'
import AddSettings from './AddSettings'

export {
	ManageUsers,
	ManageSettings,
	AddSettings
}