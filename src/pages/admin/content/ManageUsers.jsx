import { UsersCard } from '../../../components';
 
const ManageUsers = () => {
  return (
    <>
      <div className="grid grid-cols-3 gap-3">
         <UsersCard />
         <UsersCard />
         <UsersCard />
         <UsersCard />
      </div>
    </>
  );
}

export default ManageUsers