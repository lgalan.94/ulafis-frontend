import { Card, Typography, CardBody, Input, Textarea, Select, Option } from '@material-tailwind/react';
import { CustomTitle, DefaultNavbar, DefaultSidebar2, Layout, CustomButton, CustomDialogBox, CustomAlert } from '../../../components';
import { IoMdArrowBack } from "react-icons/io";
import { GrUpdate } from "react-icons/gr";
import { FcUndo } from "react-icons/fc";
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ImSpinner2 } from "react-icons/im";

const AddSettings = () => {

	let navigate = useNavigate();
	const [key, setKey] = useState('');
	const [value, setValue] = useState('');
	const [openAlert, setOpenAlert] = useState(false);
	const [openAlert2, setOpenAlert2] = useState(false);
	const [keyAlert, setKeyAlert] = useState(false);
	const [valueAlert, setValueAlert] = useState(false);
	const [isError, setIsError] = useState(false);
	const [isError2, setIsError2] = useState(false);
	const [isDisabled, setIsDisabled] = useState(false);
	const [isClicked, setIsClicked] = useState(false);

	useEffect(() => {
				if (key.length > 0) {
							setIsError(false);
							setKeyAlert(false)
				}
				if (value.length > 0) {
							setIsError2(false);
							setValueAlert(false)
				}
	}, [key, value])

	useEffect(() => {
					if (key.length > 15) {
								setIsError(true);
								setOpenAlert(true);
								setIsDisabled(true);
					} else {
								setIsError(false);
								setOpenAlert(false);
								setIsDisabled(false);
					}

					if (value.length > 50) {
								setIsError2(true);
								setOpenAlert2(true);
								setIsDisabled(true);
					} else {
								setIsError2(false);
								setOpenAlert2(false);
								setIsDisabled(false);
					}
	}, [key, value])

	useEffect(() => {
				if (key.length > 15 && value.length < 50) {
						setIsDisabled(true)
				} else if (key.length <= 15 && value.length > 50 ) {
						setIsDisabled(true)
				} else if (key.length > 15 && value.length > 50 ) {
						setIsDisabled(true)
				} else {
						setIsDisabled(false)
				}
	}, [key, value])

	const handleSave = (e) => {
				e.preventDefault();
				setIsClicked(true);

				if (key.length === 0) {
							setIsError(true);
							setKeyAlert(true);
				}
				if (value.length === 0) {
							setIsError2(true);
							setValueAlert(true);
				}

				fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings/add`, {
							method: "POST",
							headers: {
									'Content-type': 'application/json'
							},
							body: JSON.stringify({
										key: key.toUpperCase(),
										value: value
							})
				})
				.then(response => response.json())
				.then(data => {
						console.log(data)
							if (data === true) {
										toast.success(`${key} Successfully added!`);
										setIsClicked(false);
										setTimeout(() => navigate('/ulafis/admin/settings'), 1000)
							} else {
										toast.error('Error adding data!')
										setIsClicked(false);
							}
				})
	}

	  return (
	  		<>
	  				<CustomTitle title="ADMIN | Settings | Add" />
	  				<DefaultNavbar userRole="administrator" />
	  				<div className="flex flex-row">
	  						<DefaultSidebar2 />
											<Layout
													layout="w-full"
													component1={
															<CustomButton
																	icon={<IoMdArrowBack />}
																	label="back"
																	variant="text"
																	btnClass="lowercase"
																	handleClick={() => navigate('/ulafis/admin/settings')}
															/>
													}
											>
										  		<Card floated={true} shadow={true} className="group shadow bg-light">
										  		  <CardBody className="flex flex-col items-center justify-center gap-4">
										  		  				<CustomAlert
										  		  			   		alertLabel="Maximum of 15 characters only!"
										  		  			   		openAlert={openAlert}
										  		  			   />
								  		  			  	<CustomAlert
								  		  			     		alertLabel="Key is required!"
								  		  			     		openAlert={keyAlert}
								  		  			     />
								  		  						<Input
								  		  						  type="text"
								  		  						  size="md"
								  		  						  value={key.toUpperCase()}
								  		  						  onChange={(e) => setKey(e.target.value)}
								  		  						  className={`capitalize`}
								  		  						  label={`KEY (${key.length})`}
								  		  						  error={isError}
								  		  						/>
								  		  							<CustomAlert
								  		  						   		alertLabel="Maximum of 50 characters only!"
								  		  						   		openAlert={openAlert2}
								  		  						   />
						  		  						  	<CustomAlert
						  		  						     		alertLabel="Value is required!"
						  		  						     		openAlert={valueAlert}
						  		  						     />

						  		  						   <Input
						  		  						     type="text"
						  		  						     size="md"
						  		  						     value={value}
						  		  						     onChange={(e) => setValue(e.target.value)}
						  		  						     className={`capitalize`}
						  		  						     label={`VALUE (${value.length})`}
						  		  						     error={isError2}
						  		  						   />
						  		  						     
										  		  		  {/*<div className="w-full">
						  		  		        <Textarea 
						  		  		        		label={`VALUE (${value.length})`} 
						  		  		        		value={value}
						  		  		        		onChange={(e) => setValue(e.target.value)}
						  		  		        		error={isError2}
						  		  		        		rows={1}
						  		  		        		className="max-h-[20px]"
						  		  		        />
						  		  		      </div>*/}
						  		  		      

										  		  		<div className="flex flex-row gap-1 mt-10">
										  		  				
										  		  					<CustomButton
										  		  							icon={<FcUndo />}
										  		  							label="clear"
										  		  							variant="filled"
										  		  							handleClick={() => {setKey(''); setValue('')}}
										  		  					/>

										  		  					{
										  		  							!isClicked ? (
							  		  													<CustomButton
							  		  															icon={<GrUpdate />}
							  		  															label="Save"
							  		  															color="blue"
							  		  															variant="filled"
							  		  															handleClick={handleSave}
							  		  															isDisabled={isDisabled}
							  		  													/>
										  		  								) : (
										  		  										<CustomButton
										  		  												icon={<ImSpinner2 className="animate-spin" />}
										  		  												label="Saving..."
										  		  												color="blue"
										  		  												variant="filled"
										  		  												isDisabled={true}
										  		  										/>
										  		  								)
										  		  					}

										  		  		</div>
										  		    
										  		  </CardBody>
										  		</Card>
											</Layout>
	  				</div>
	  		</>
	  )
}

export default AddSettings