import { SettingsCard } from '../../../components';
import { useState, useEffect } from 'react';
import { ImSpinner2 } from "react-icons/im";

const ManageSettings = () => {

	const [settings, setSettings] = useState([]);
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings`)
				.then(response => response.json())
				.then(data => {
							if (data.length > 0) {
										setSettings(data.map(settings => {
													return (
																	<SettingsCard key={settings._id} SettingsProp={settings} />
														)
										}))
										setIsLoading(false);
							} else {
										setSettings(<div>No data</div>)
										setIsLoading(false);
							}
				})
	}, [settings])

			return (
						<>
								{
										!isLoading ? (
															<div className="grid grid-cols-4 gap-3">
																			{settings}			
															</div>
											) : (
														<ImSpinner2 className="w-12 h-12 min-h-[67vh] mx-auto animate-spin" />
											)
								}
						</>
			)
}

export default ManageSettings