import { DefaultNavbar, DefaultSidebar2, Layout, CustomTitle } from '../../components';
import { ManageUsers } from './content'

const Users = () => {
		return (
						<>
								<CustomTitle title="ADMIN | Manage Users " />
								<DefaultNavbar userRole="administrator" />
								<div className="flex flex-row">
										<DefaultSidebar2 />
															<Layout
																	layout="w-full"
																	
															>
																<ManageUsers />
															</Layout>
								</div>

						</>
			)
}

export default Users