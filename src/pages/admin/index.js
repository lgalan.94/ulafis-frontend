import Dashboard from './Dashboard'
import Users from './Users'
import Reports from './Reports'
import Settings from './Settings'

export {
	Dashboard,
	Users,
	Reports,
	Settings
}