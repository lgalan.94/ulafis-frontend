import { DefaultNavbar, DefaultSidebar2, Layout, CustomTitle } from '../../components';
import { Typography } from '@material-tailwind/react';

const Reports = () => {
		return (
						<>
								<CustomTitle title="ADMIN | Reports " />
								<DefaultNavbar userRole="administrator" />
								<div className="flex flex-row">
										<DefaultSidebar2 />
															<Layout
																	layout="w-full"
																	
															>
																<Typography variant="h1">Reports Page</Typography>
															</Layout>
								</div>

						</>
			)
}

export default Reports