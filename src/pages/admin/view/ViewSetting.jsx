import { Card, Typography, CardBody, Input, Textarea, Select, Option } from '@material-tailwind/react';
import { CustomTitle, DefaultNavbar, DefaultSidebar2, Layout, CustomButton, CustomDialogBox } from '../../../components';
import { IoMdArrowBack } from "react-icons/io";
import { MdDelete } from "react-icons/md";
import { GrUpdate } from "react-icons/gr";
import { FcUndo } from "react-icons/fc";
import { useNavigate, useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ViewSetting = () => {

	let navigate = useNavigate();
	let {settingsId} = useParams();

	let [key, setKey] = useState('');
	let [value, setValue] = useState('');
	let [isActive, setIsActive] = useState();

	const [open, setOpen] = useState(false);
	const handleOpen = () => setOpen(!open); 

	useEffect(() => {
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings/${settingsId}`)
					.then(response => response.json())
					.then(data => {
								setKey(data.key);
								setValue(data.value);
								setIsActive(data.isActive);
					})
	}, [])

	const handleUpdate = (e) => {
				e.preventDefault();
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings/${settingsId}`, {
							method: 'PATCH',
							headers: {
									'Content-type': 'application/json'
							},
							body: JSON.stringify({
										key,
										value,
										isActive
							})
				})
				.then(response => response.json())
							.then(data => {
										if (data === true) {
													toast.success(`Successfully updated!`);
													setTimeout(() => navigate('/ulafis/admin/settings'), 1000)
										} else {
													toast.error('Error updating data!')
										}
							})
	}

	const handleDelete = (e) => {
				e.preventDefault();
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings/${settingsId}`, {
							method: 'DELETE'
				})
				.then(response => response.json())
							.then(data => {
										if (data === true) {
													setTimeout(() => navigate('/ulafis/admin/settings'), 1000)
													toast.success(`Successfully Deleted!`);
										} else {
													toast.error('Error Deleting data!')
										}
							})
	}

	  return (
	  		<>
	  				<CustomTitle title="ADMIN | Settings | Update" />
	  				<DefaultNavbar userRole="administrator" />
	  				<div className="flex flex-row">
	  						<DefaultSidebar2 />
											<Layout
													layout="w-full"
													component1={
															<CustomButton
																	icon={<IoMdArrowBack />}
																	label="back"
																	variant="text"
																	btnClass="lowercase"
																	handleClick={() => navigate('/ulafis/admin/settings')}
															/>
													}
											>
										  		<Card floated={true} shadow={true} className="group shadow bg-light">
										  		  <CardBody className="flex flex-col items-center justify-center gap-4">
								  		  						<Input
								  		  								disabled
								  		  						  type="text"
								  		  						  size="md"
								  		  						  value={key.toUpperCase()}
								  		  						  onChange={(e) => setKey(e.target.value)}
								  		  						  className={`capitalize`}
								  		  						  label="KEY"
								  		  						/>
										  		  		  <div className="w-full">
						  		  		        <Textarea 
						  		  		        		label="VALUE" 
						  		  		        		value={value}
						  		  		        		onChange={(e) => setValue(e.target.value)}
						  		  		        />
						  		  		      </div>
						  		  		      <div className="flex flex-row self-start gap-2">
  						  		  		      <div className="w-60">
  		  		  		            <Select onChange={(e) => setIsActive(e)} variant="outlined" label="Status">
  		  		  		              <Option value={true} >Set Active</Option>
  		  		  		              <Option value={false}>Set Inactive</Option>
  		  		  		            </Select>
  		  		  		          </div>
  		  		  		          <Typography className="text-sm italic font-semibold self-center text-cyan-600">{isActive ? 'Active' : <span className="text-gray-500">Inactive</span>}</Typography>
						  		  		      </div>

										  		  		<div class="flex flex-row gap-1 mt-10">
										  		  					<CustomButton
										  		  							icon={<MdDelete />}
										  		  							label="Delete"
										  		  							variant="filled"
										  		  							color="red"
										  		  							handleClick={handleOpen}
										  		  					/>

										  		  					<CustomButton
										  		  							icon={<FcUndo />}
										  		  							label="clear"
										  		  							variant="filled"
										  		  							handleClick={() => navigate('/ulafis/admin/settings')}
										  		  					/>

										  		  					<CustomButton
										  		  							icon={<GrUpdate />}
										  		  							label="update"
										  		  							color="blue"
										  		  							variant="filled"
										  		  							handleClick={handleUpdate}
										  		  					/>

										  		  		</div>
										  		    
										  		  </CardBody>
										  		</Card>
											</Layout>
	  				</div>

	  				<CustomDialogBox
	  						size="xs"
	  						open={open}
	  						handleOpen={handleOpen}
	  						title="Confirmation"
	  						closeButton={handleOpen}
	  				>
	  							Delete {key}?

	  							<div class="flex flex-row gap-1 justify-center mt-10">
	  										<CustomButton
	  												label="Cancel"
	  												variant="filled"
	  												handleClick={handleOpen}
	  										/>

	  										<CustomButton
	  												label="Delete"
	  												color="red"
	  												variant="filled"
	  												handleClick={handleDelete}
	  										/>
	  							</div>

	  				</CustomDialogBox>
	  		</>
	  )
}

export default ViewSetting