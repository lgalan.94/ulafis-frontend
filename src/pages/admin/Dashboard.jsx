import { DefaultNavbar, DefaultSidebar2, Layout, CustomTitle } from '../../components';
import { Typography } from '@material-tailwind/react';

const Dashboard = () => {
		return (
						<>
								<CustomTitle title="ADMIN | Dashboard " />
								<DefaultNavbar userRole="administrator" />
								<div className="flex flex-row">
										<DefaultSidebar2 />
															<Layout
																	layout="w-full"
																	
															>
																<Typography variant="h1">ADMIN Dashboard</Typography>
															</Layout>
								</div>

						</>
			)
}

export default Dashboard