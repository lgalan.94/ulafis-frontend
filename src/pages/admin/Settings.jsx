import { DefaultNavbar, DefaultSidebar2, Layout, CustomTitle, CustomButton } from '../../components';
import { ManageSettings } from './content';
import { IoMdAdd } from "react-icons/io";
import { useNavigate } from 'react-router-dom';

const Reports = () => {

	let navigate = useNavigate();
		return (
						<>
								<CustomTitle title="ADMIN | Settings " />
								<DefaultNavbar userRole="administrator" />
								<div className="flex flex-row">
										<DefaultSidebar2 />
															<Layout
																	layout="w-full"
																	component1={
																				<CustomButton
																						label="add"
																						variant="text"
																						btnClass="lowercase"
																						icon={<IoMdAdd />}
																						handleClick={() => navigate('/ulafis/admin/settings/add')}
																				/>
																	}
															>
																<ManageSettings />
															</Layout>
								</div>

						</>
			)
}

export default Reports