import { DefaultNavbar, DefaultSidebar, Layout, CustomTitle } from '../components';
import { Typography } from '@material-tailwind/react';

const Home = () => {
		return (
						<>
								<CustomTitle title="U-LAFIS | Home" />
								<DefaultNavbar userRole="lab manager" />
								<div className="flex flex-row">
										<DefaultSidebar />
															<Layout
																	layout="w-full"
																	
															>
																<Typography variant="h1">Lab Manager Home Page</Typography>
															</Layout>
								</div>

						</>
			)
}

export default Home