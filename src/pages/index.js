import Home from './Home';
import Landing from './Landing'
import UpdateItem from './UpdateItem'
import Categories from './Categories'
import UpdateCategory from './UpdateCategory'
import Notifications from './Notifications'
import Inventory from './Inventory'

export {
	Home,
	Landing,
	UpdateItem,
	Categories,
	UpdateCategory,
	Notifications,
	Inventory
}