import { DefaultNavbar, Layout, DefaultSidebar, ItemCard, CustomButton, CustomAlert, CustomTitle } from '../components';
import { CategoryChild } from './content';
import { Typography } from '@material-tailwind/react';
import { useState, useEffect } from 'react';
import { ImSpinner2 } from "react-icons/im";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';

const Categories = () => {

	let no_image = "";

	const [nameAlert, setNameAlert] = useState(false);
	const [imageUrlAlert, setImageUrlAlert] = useState(false);
	const [image_url, set_image_url] = useState(no_image);
	const [name, setName] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [isClicked, setIsClicked] = useState(false);

	useEffect(() => {
			if (name.length !== 2) {
						setNameAlert(false);
			}
			if (name.length > 0 && image_url.length > 0) {
						setIsDisabled(false);
			} else {
						setIsDisabled(true);
			}
	}, [name, image_url])


	const handleSave = (e) => {
			e.preventDefault();
			setIsClicked(true);
			setIsDisabled(true);
			if (name.length < 3) {
						setNameAlert(true);
			}
			if (image_url.length === 0) {
						setImageUrlAlert(true);
			}
			fetch(`${import.meta.env.VITE_API_URL}/ulafis/category/add`, {
					method: 'POST',
					headers: {
							'Content-type': 'application/json'
					},
					body: JSON.stringify({
							name: name.toUpperCase(),
							image_url: image_url
					})
			})
			.then(response => response.json())
			.then(data => {
						if (data === true) {
									toast.success(`${name} Successfully added!`)
									setName('');
									set_image_url(no_image);
									setIsDisabled(true);
									setIsClicked(false);
						} else {
									toast.error('Unable to add category!')
									setName('');
									set_image_url(no_image);
									setIsDisabled(true);
									setIsClicked(false);
						}
			})
	}
	const handleClear = () => {
				setName('');
				set_image_url('');
	}

		return (
				<>
					<CustomTitle title="U-LAFIS | Categories" />
					<DefaultNavbar userRole="lab manager" />
					<div className="flex flex-row">
							<DefaultSidebar />
							<Layout
									layout="w-[70vw] !px-2"
									component1={
												<Typography className="text-sm font-bold">LIST OF CATEGORIES</Typography>
									}
							>
									<CategoryChild />
							</Layout>
							<Layout
									layout="w-[30vw]"
									layoutHeader="!justify-center"
									component1={
												<Typography className="text-sm font-bold">
														ADD Category
												</Typography>
									}
							>

								<ItemCard
										imageUrl={image_url}
										name={name.toUpperCase()}
										image_url={image_url}
										handleNameChange={(e) => setName(e.target.value)}
										handleImageUrlChange={(e) => set_image_url(e.target.value)}
										inputNameClass="mt-3"
										inputDescriptionClass="hidden"
										inputQuantityClass="hidden"
										inputLowStockThresholdClass="hidden"
										inputLocationClass="hidden"
										inputCategoryClass="hidden"
										divClass="!flex-col"
										displayAlert={
													<>
														<CustomAlert
																openAlert={nameAlert}
																alertLabel="Name must be greater than 2 characters!"
														/>
														<CustomAlert
																openAlert={imageUrlAlert}
																alertLabel="Image Url must not be empty!"
														/>
													</>
										}
								/>

								<div className="flex flex-row justify-center mt-5 gap-1">
										<CustomButton 
												label="clear"
												btnClass="hover:scale-105"
												handleClick={handleClear}
										/>
										{
												!isClicked ? (
																<CustomButton 
																		label="save"
																		color="green"
																		btnClass="hover:scale-105"
																		handleClick={handleSave}
																		isDisabled={isDisabled}
																/>
													) : (
															<CustomButton 
																	label="saving..."
																	color="green"
																	isDisabled={isDisabled}
															/>
													)
										}
								</div>

							</Layout>

					</div>
					<div className="absolute">
					  <ToastContainer position="top-right" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
					</div>	
				</>
		)
}

export default Categories
