import { DefaultNavbar, DefaultSidebar, Layout, CustomButton, CustomTitle } from '../components';
import { ManageInventory } from './content';
import { IoMdAdd } from "react-icons/io";
import { useNavigate } from 'react-router-dom';

const Inventory = () => {

	let navigate = useNavigate();

	return (
			<>
					<CustomTitle title="U-LAFIS | Inventory" />
					<DefaultNavbar userRole="lab manager" />
					<div className="flex flex-row">
							<DefaultSidebar />
												<Layout
														layout="w-full"
														component1={
															<CustomButton 
																	label="add"  
																	icon={<IoMdAdd className="h-3 w-3" />}
																	variant="text"
																	handleClick={() => navigate('/ulafis/inventory/add-item')}
															/>
														}
														component2={
													  <input
							          type="text"
							          placeholder="Search item here..."
							          className="ml-4 rounded-md px-3 py-1"
							        />
														}
												>
													<ManageInventory />
													
												</Layout>
					</div>

			</>
	)
}

export default Inventory