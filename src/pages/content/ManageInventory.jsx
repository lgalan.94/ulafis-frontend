import { useState, useEffect } from 'react';
import { ItemsList } from '../../components';
import { Card, Typography } from '@material-tailwind/react';
import { ImSpinner2 } from "react-icons/im";

const ManageInventory = () => {

	const TABLE_HEAD = ["Image", "Name", "Category", "Description", ""];
	const [items, setItems] = useState([]);
	const [count, setCount] = useState(0);
	const [loading, setLoading] = useState(true)

	useEffect(() => {
				fetch(`${import.meta.env.VITE_API_URL}/ulafis/items/all`, {
						method: 'GET',
						headers: {
								'Content-type': 'application/json'
						}
				})
				.then(response => response.json())
				.then(data => {
							if (data.length > 0) {
									setItems(data.map(item => {
										return (
													<ItemsList key={item._id} itemProps={item} />
											)
									}))
									setCount(data.length);
									setLoading(false);
							} else {
									setItems(<Typography variant="h5" className="italic text-dark/75">No items in the database!</Typography>)
									setCount(0);
									setLoading(false);
							}				
				})
	}, [items, count])

	return (
			<>
						<div className="border border-2 rounded-md h-full min-h-[68vh] max-h-[68vh] overflow-y-auto scrollbar-thin">
								<Card className="shadow-none">
					      {
					      		!loading ? (
					      						<table className="w-full min-w-max table-auto text-left">
					      						  <thead className="sticky top-0">
					      						    <tr>
					      						      {TABLE_HEAD.map((head) => (
					      						        <th
					      						          key={head}
					      						          className="border-b border-blue-gray-100 bg-blue-gray-50 py-4"
					      						        >
					      						          <Typography
					      						            variant="small"
					      						            color="blue-gray"
					      						            className="font-normal leading-none opacity-70"
					      						          >
					      						            {head}
					      						          </Typography>
					      						        </th>
					      						      ))}
					      						    </tr>
					      						  </thead>
					      						  <tbody>
					      						   	{items}
					      						  </tbody>
					      						</table>
					      			) : (
					      					<ImSpinner2 className="w-12 h-12 min-h-[67vh] mx-auto animate-spin" />
					      			)
					      }
					    </Card>
						</div>

						<div className="absolute text-xs italic mt-2">
								{count} items
						</div>

			</>
	)
}

export default ManageInventory