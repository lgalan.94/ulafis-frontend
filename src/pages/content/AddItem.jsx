import { CustomAlert, CustomTitle, Layout, DefaultNavbar, DefaultSidebar, ItemCard, CustomButton } from '../../components';
import { useState, useEffect } from 'react';
import { ImSpinner2 } from "react-icons/im";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Tooltip } from '@material-tailwind/react';
import { useNavigate } from 'react-router-dom';
import { IoArrowBack } from "react-icons/io5";
import { LuSaveAll } from "react-icons/lu";
import { RiRefreshFill } from "react-icons/ri";

const AddItem = () => {

	let url = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMtaN6L0FkLLyNRmuaul-Vdh6MIt9P5M5bqpihnmWpzw&s';

	let navigate = useNavigate();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [quantity, setQuantity] = useState('');
	const [location, setLocation] = useState('');
	const [category, setCategory] = useState('');
	const [image_url, set_image_url] = useState(url);
	const [low_stock_threshold, set_low_stock_threshold] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [isClicked, setIsClicked] = useState(false);
	const [nameAlert, setNameAlert] = useState(false);
	const [descriptionAlert, setDescriptionAlert] = useState(false);
	const [quantityAlert, setQuantityAlert] = useState(false);
	const [lowStockAlert, setLowStockAlert] = useState(false);
	const [locationAlert, setLocationAlert] = useState(false);
	const [categoryAlert, setCategoryAlert] = useState(false);
	const [imageUrlAlert, setImageUrlAlert] = useState(false);
	const [thresholdAlert, setThresholdAlert] = useState(false);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
			setTimeout(() => setLoading(false), 700)
	}, [])

	const handleAdd = (e) => {
			e.preventDefault();
			setIsClicked(true);
			if (name.length < 1) {
							setNameAlert(true)
			}
			if (quantity.length < 1) {
							setQuantityAlert(true)
			}
			if (low_stock_threshold.length < 1) {
							setLowStockAlert(true)
			}
			if (location.length < 1) {
							setLocationAlert(true)
			}
			if (category.length < 1) {
							setCategoryAlert(true)
			}
			if (image_url.length < 1) {
							setImageUrlAlert(true)
			}
			if (description.length < 1) {
							setDescriptionAlert(true)
			}

			fetch(`${import.meta.env.VITE_API_URL}/ulafis/items/add`, {
					method: 'POST',
					headers: {
							'Content-type': 'application/json'
					},
					body: JSON.stringify({
							name: name.toUpperCase(),
							quantity: quantity,
							low_stock_threshold: low_stock_threshold,
							location: location,
							category: category,
							image_url: image_url,
							description:description
					})
			})
			.then(response => response.json())
			.then(data => {
						if (data === true) {
									toast.success(`${name.toUpperCase()} Successfully Added!`);
									setIsClicked(false);
									setTimeout(() => navigate('/ulafis/inventory'), 1000)
						} else {
									toast.error('Unable to add new item!')
									setIsClicked(false);
						}
			})
	}

	useEffect(() => {
				if (name.length >= 1) {
							setNameAlert(false)
				}
	}, [name])
	useEffect(() => {
				if (description.length >= 1) {
							setDescriptionAlert(false)
				}
	}, [description])
	useEffect(() => {
				if (low_stock_threshold.length >= 1) {
							setLowStockAlert(false)
				}
	}, [low_stock_threshold])
	useEffect(() => {
				if (location.length >= 1) {
							setLocationAlert(false)
				}
	}, [location])
	useEffect(() => {
				if (category.length >= 1) {
							setCategoryAlert(false)
				}
	}, [category])
	useEffect(() => {
				if (image_url.length >= 1) {
							setImageUrlAlert(false)
				}
	}, [image_url])
	useEffect(() => {
				if (quantity.length >= 1) {
							setQuantityAlert(false)
				}
	}, [quantity])

	const handleNameChange = (e) => {
		setName(e.target.value);
	}
	const handleDescriptionChange = (e) => {
		setDescription(e.target.value);
	}
	const handleQuantityChange = (e) => {
		setQuantity(e.target.value);
	}
	const handleLocationChange = (e) => {
		setLocation(e.target.value);
	}
	const handleCategoryChange = (e) => {
		setCategory(e);
	}
	const handleImageUrlChange = (e) => {
		set_image_url(e.target.value);
	}
	const handleLowStockThreshold = (e) => {
		set_low_stock_threshold(e.target.value);
	}
	const handleClear = () => {
			setName('');
			setDescription('');
			setQuantity('');
			setLocation('');
			setCategory('');
			set_image_url('');
			set_low_stock_threshold('');
	}

	useEffect(() => {
	  if (parseInt(low_stock_threshold) >= parseInt(quantity)) {
	    setThresholdAlert(true);
	    setIsDisabled(true);
	  } else {
	    setThresholdAlert(false);
	    setIsDisabled(false);
	  }
	}, [low_stock_threshold, quantity]);

	const refresh = () => {
			setLoading(true);
			setTimeout(() => setLoading(false), 1000)
			setName('');
			setDescription('');
			setQuantity('');
			setLocation('');
			setCategory('');
			set_low_stock_threshold('');
			set_image_url(url);
	}	


			return (
						<>
								<CustomTitle title="U-LAFIS | Inventory | Add" />
								<DefaultNavbar userRole="lab manager" />
								<div className="flex flex-row">
										<DefaultSidebar />
										<Layout
												layout="w-full"
												component1={
															<CustomButton
																	label="back"
																	variant="text"
																	icon={<IoArrowBack className="h-3 w-3" />}
																	btnClass="lowercase"
																	handleClick={() => navigate('/ulafis/inventory')}
															/>
												}
												component2={
															<Tooltip 
																	placement="left"
																	content="refresh"
															  className={`bg-cyan-600 text-xs`}
															>
																	<button onClick={refresh} className={`flex self-center hover:scale-105 hover:shadow-lg hover:z-10 rounded-full text-cyan-600`}> 
																	    <RiRefreshFill className='w-8 h-8' /> 
																	</button>
															</Tooltip>
												}
										>			
												{
														!loading ? (
																		<>
																				<ItemCard
																					imageUrl={image_url}
																					name={name.toUpperCase()}
																					description={description}
																					quantity={quantity}
																					location={location}
																					category={category}
																					categoryLabel="Select Category"
																				
																					image_url={image_url}
																					lowStockThreshold={low_stock_threshold}

																					handleNameChange={handleNameChange}
																					handleDescriptionChange={handleDescriptionChange}
																					handleQuantityChange={handleQuantityChange}
																					handleLocationChange={handleLocationChange}
																					handleCategoryChange={handleCategoryChange}
																					handleImageUrlChange={handleImageUrlChange}
																					handleLowStockThreshold={handleLowStockThreshold}

																					displayAlert={
																								<>
																									<CustomAlert
																											openAlert={nameAlert}
																											alertLabel="Name is required!"
																									/>
																									<CustomAlert
																											openAlert={descriptionAlert}
																											alertLabel="Description is required!"
																									/>
																									<CustomAlert
																											openAlert={quantityAlert}
																											alertLabel="Quantity is required!"
																									/>
																									<CustomAlert
																											openAlert={lowStockAlert}
																											alertLabel="Low Stock Threshold is required!"
																									/>
																									<CustomAlert
																											openAlert={locationAlert}
																											alertLabel="Location is required!"
																									/>
																									<CustomAlert
																											openAlert={categoryAlert}
																											alertLabel="Category is required!"
																									/>
																									<CustomAlert
																											openAlert={imageUrlAlert}
																											alertLabel="Image Url is required!"
																									/>
																									<CustomAlert
																											openAlert={thresholdAlert}
																											alertLabel="Low Stock Threshold value must be lower than the number of quantity! "
																									/>
																								</>
																					}

																				/>
																				<div className="mt-8 flex flex-row gap-1 justify-center mx-auto">
																				    
																				    <CustomButton
																				      label="Clear"
																				      handleClick={handleClear}
																				      btnClass="rounded px-3 py-2 hover:scale-105"
																				    />
																				    {
																				      !isClicked ? (
																				         <CustomButton
																				           label="Save"
																				           icon={<LuSaveAll />}
																				           handleClick={handleAdd}
																				           btnClass="rounded px-3 py-2 hover:scale-105"
																				           color="green"
																				           isDisabled={isDisabled}
																				         />
																				       ) : (
																				         <CustomButton
																				           label="Saving..."
																				           btnClass="rounded px-3 py-2 hover:scale-105"
																				           color="green"
																				           icon={<ImSpinner2 className="animate-spin" />}
																				           isDisabled={true}
																				         />
																				       )
																				    }
																				</div>
																		</>
															) : (
																		<ImSpinner2 className="w-12 h-12 min-h-[67vh] mx-auto animate-spin" />
															)
												}

										</Layout>
								</div>
						</>
			)
}

export default AddItem