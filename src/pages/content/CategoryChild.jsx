import { CategoryCard } from '../../components';
import { useState, useEffect } from 'react';
import { Typography } from '@material-tailwind/react';
import { ImSpinner2 } from "react-icons/im";

const CategoryChild = ({handleUpdate}) => {

	const [categories, setCategories] = useState([]);
	const [loading, setLoading] = useState(true);

useEffect(() => {
		fetch(`${import.meta.env.VITE_API_URL}/ulafis/category`, {
					method: 'GET',
					headers: {
							'Content-type': 'application/json'
					}
		})
		.then(response => response.json())
		.then(data => {
					if (data.length > 0) {
							setCategories(data.map(category => {
									return (
												<CategoryCard handleUpdate={handleUpdate} key={category._id} CategoriesProps={category} />
									)
							}))
							setLoading(false);
					} 
					else 
					{
							setLoading(false);
							setCategories(<Typography variant="h5">No data in the database!</Typography>)
					}
		})
}, [categories])


			return(
						<>
						{
							 loading ? (
							 					<ImSpinner2 className="w-12 h-12 min-h-[67vh] mx-auto animate-spin" />
							 	) : (
							 				<div className="grid grid-cols-2 lg:grid-cols-3 gap-3">
							 							{categories}
							 				</div>
							 	)
						} 
						</>
			)	
}

export default CategoryChild