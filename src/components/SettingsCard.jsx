import { Card, CardBody, Typography } from '@material-tailwind/react';
import { InvisibleButton } from './';
import { MdDeleteOutline } from "react-icons/md";
import { IoMdEye } from "react-icons/io";
import { useNavigate } from 'react-router-dom';

const SettingsCard = (props) => {

	const { _id, key, value } = props.SettingsProp;

	let navigate = useNavigate();

			return (
					<Card floated={true} shadow={true} className="group shadow bg-light hover:scale-105">
					  <CardBody className="flex flex-col items-center justify-center gap-4">
					    <Typography className="shadow-sm text-sm font-bold tracking-wider bg-defaultColor px-3 py-1 rounded-full">
					      	{key.substring(0, 15)}
					    </Typography>
					    <Typography className="text-sm flex items-center justify-center font-normal bg-defaultColor rounded-sm h-full max-h-[3rem] min-h-[3rem] w-full">
					      	{value.substring(0, 20) + "..."}
					    </Typography>
					  </CardBody>
					  <InvisibleButton
					  		icon={<IoMdEye className="h-5 w-5" />}
					  		label="view details"
					  		handleClick={(e) => navigate(`/ulafis/admin/settings/view/${_id}`)}
					  		buttonClass="mb-2"
					  		placement="top"
					  />
					</Card>
		)
}

export default SettingsCard;