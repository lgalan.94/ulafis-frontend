import {
  Card,
  CardBody,
  Typography,
  Avatar,
} from "@material-tailwind/react";

const UsersCard = () => {
			return (
						<Card color="transparent" shadow={false} className="w-full shadow shadow-t-md hover:bg-blue-gray-50 hover:scale-105">
						  <CardBody
						    color="transparent"
						    floated={false}
						    shadow={false}
						    className="flex flex-col items-center justify-center gap-3"
						  >
						    <Avatar
						      size="xl"
						      variant="circular"
						      src="https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1480&q=80"
						      className=""
						    />
						    <div className="flex w-full flex-col justify-center items-center gap-0.5">
						      <div className="flex items-center">
						        <Typography variant="h5" color="blue-gray">
						          Tania Andrew
						        </Typography>
						      </div>
						      <Typography color="blue-gray">Lab Manager</Typography>
						    </div>
						  </CardBody>
						</Card>
			)
}

export default UsersCard;