import { Button } from '@material-tailwind/react';
import { ImSpinner2 } from "react-icons/im";

const CustomButton = ({ label, variant, color, isDisabled, icon, handleClick, btnClass }) => {
		return (
					<Button onClick={handleClick} className={`${btnClass} hover:scale-105 py-2 flex flex-row justify-center items-center gap-0.5`} disabled={isDisabled} size="sm" variant={variant} color={color}>
					  {icon} <span className="text-xs"> {label} </span>
					</Button>
		)
}

export default CustomButton
