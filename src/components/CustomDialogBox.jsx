import {
  Dialog,
  DialogHeader, 
  DialogBody,
  Button
} from "@material-tailwind/react";
import { IoMdClose } from "react-icons/io";

const CustomDialogBox = ({ children, title, open, handleOpen, closeButton, size, buttonClass }) => {
	 return (
	 			<Dialog 
	 			  size={size} 
	 			  open={open} 
	 			  handler={handleOpen}
	 			  animate={{
	 			    mount: { scale: 1, y: 0 },
	 			    unmount: { scale: 0.9, y: -100 },
	 			  }}
	 			>
	 			  <DialogHeader className="border border-b-gray-900/20 rounded-t-lg shadow justify-center"> 
	 			  		{title}  
	 			  		
	 			  		<span className="absolute top-0 right-0">
	 			  			<Button className={`${buttonClass}`} onClick={closeButton} variant="text">
	 			  				<IoMdClose className="w-5 h-5 font-bold" />
	 			  			</Button>
	 			  		</span>

	 			  </DialogHeader>
	 			  <DialogBody className="text-center font-leading">
	 			    
	 			    {children}
	 			  		
	 			  </DialogBody>
	 			</Dialog>
	 )
}

export default CustomDialogBox