import { Card, Chip, Typography, Button } from '@material-tailwind/react';
import { IoArrowBackOutline } from "react-icons/io5";
import { DefaultNavbar } from './';
import { ToastContainer } from 'react-toastify';

const Layout = ({ layout, children, handleClick, component1, component2, layoutHeader, card, layoutHeight }) => {
		return (
					<>
							<div className={`${layout} py-5 mx-auto pr-0 lg:pr-10 h-full`}> 
									<Card className={`${card} bg-white`}>
										<div className={`${layoutHeader} min-h-[35px] flex flex-row justify-between max-h-[35px] flex items-center p-1 border shadow-none rounded-t-lg border-b-gray-900/10 gap-x-1`}>
													<div>
															{component1}
													</div>

													<div>
															{component2}
													</div>
										</div>
										<div className={`${layoutHeight} px-10 py-6 h-full w-full max-h-[92vh] min-h-[92vh] lg:max-h-[78vh] lg:min-h-[78vh] overflow-y-auto scrollbar-thin scrollbar-thumb-dark/30 scrollbar-track-gray-100 mx-auto `}>
												{children}
										</div>
									</Card>	
							</div>
							<div className="absolute">
							  <ToastContainer position="top-right" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
							</div>	
					</>
		)
}

export default Layout;