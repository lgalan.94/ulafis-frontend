
const Banner = ({ headline, title, subheading }) => {
	return (
		<div className="h-full min-h-[69vh] w-full flex flex-col justify-center">
			<div className="text-4xl font-bold capitalize ">
				{headline}
			</div>
			
			<div className="text-7xl font-semibold uppercase bg-orange-600 rounded-lg py-3 text-light">
				{title}
			</div>
			
			<div className="italic tracking-wide text-dark/60">
				{subheading}
			</div>
		</div>
	)
}

export default Banner