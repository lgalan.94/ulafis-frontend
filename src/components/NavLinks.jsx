import { List, ListItem, ListItemPrefix, ListItemSuffix, Typography, Chip, Accordion, AccordionHeader, AccordionBody } from '@material-tailwind/react';
import { Link, useLocation } from 'react-router-dom'
import { TbCategoryPlus } from "react-icons/tb";
import { FiHome } from "react-icons/fi";
import { IoMdAdd } from "react-icons/io";
import { MdNotificationsActive } from "react-icons/md";
import { MdOutlineInventory } from "react-icons/md";
import { useState, useEffect } from 'react';
import { SiManageiq } from "react-icons/si";

import {
  PresentationChartBarIcon,
  ShoppingBagIcon,
  UserCircleIcon,
  Cog6ToothIcon,
  InboxIcon,
  PowerIcon,
} from "@heroicons/react/24/solid";
import { ChevronRightIcon, ChevronDownIcon } from "@heroicons/react/24/outline";

const NavLinks = () => {

	let currentPath = useLocation().pathname;
	let categoryPath = '/ulafis/categories';
	let inventoryPath = '/ulafis/inventory';
	let addItemPath = '/ulafis/inventory/add-item';

	const [open, setOpen] = useState(0); 
 const handleOpen = (value) => {
   setOpen(open === value ? 0 : value);
 };

	const CustomLink = ({ name, linkIcon, linkPath, className, chipValue, chipClassname, listItem  }) => {

	  return (
	    <Link 
	      as={Link}
	      to={linkPath}
	      className={`${className} ${currentPath === linkPath ?  'bg-light shadow-sm rounded-full' : 'bg-none' } `}
	    >
	      <ListItem className={`py-1 hover:shadow-sm hover:rounded-full hover:scale-105 ${listItem}`}>
	        <ListItemPrefix>
	          {linkIcon}
	        </ListItemPrefix>
	        {name}
	        <ListItemSuffix>
            <span className={`${chipClassname} text-xs bg-gray-100 px-2 py-1 rounded-full`}>{chipValue}</span>
          </ListItemSuffix>
	      </ListItem>
	    </Link>
	  )
	}

	const CustomAccordion = ({ children, title, icon, a_value }) => {
				return (
						<Accordion
			     open={open === a_value}
			     icon={
			       <ChevronDownIcon
			         strokeWidth={2.5}
			         className={`mx-auto h-4 w-4 transition-transform ${open === a_value ? "rotate-180" : ""}`}
			       />
			     }
			   >
			     <ListItem className="p-0" selected={open === a_value}>
			       <AccordionHeader onClick={() => handleOpen(a_value)} className="border-b-0 px-3 py-1">
			         <ListItemPrefix>
			           {icon}
			         </ListItemPrefix>
			         <Typography color="blue-gray" className="mr-auto font-normal">
			           {title}
			         </Typography>
			       </AccordionHeader>
			     </ListItem>
			     <AccordionBody className="py-1">
			       <List className="ml-3 p-0">
			         	{children}
			       </List>
			     </AccordionBody>
			   </Accordion>
				)
	}

	return (
		<>
			<List className="">
					<CustomLink 
						name="Dashboard" 
						linkIcon={<PresentationChartBarIcon className="h-5 w-5" />} 
						linkPath="/ulafis/home" 
						chipClassname="hidden"

					/>

					<CustomAccordion
							title="Inventory"
							icon={<MdOutlineInventory className="h-5 w-5" />}
							a_value={
										currentPath === inventoryPath || currentPath === addItemPath ? 0 : 1
							}
					>	
							<CustomLink 
								name="Manage Items" 
								linkIcon={<SiManageiq strokeWidth={3} className="h-3 w-5" />} 
								linkPath="/ulafis/inventory" 
								chipClassname="hidden"
								listItem="text-xs"
							/>
							<CustomLink 
								name="Add Item" 
								linkIcon={<IoMdAdd strokeWidth={3} className="h-3 w-5" />} 
								linkPath="/ulafis/inventory/add-item" 
								chipClassname="hidden"
								listItem="text-xs"
							/>
					</CustomAccordion>

					<CustomAccordion
							title="Categories"
							icon={<TbCategoryPlus className="w-5 h-5" />}
							a_value={
										currentPath === categoryPath ? 0 : 2
							}
					>
							<CustomLink 
								name="Categories" 
								linkIcon={<ChevronRightIcon strokeWidth={3} className="h-3 w-5" />} 
								linkPath="/ulafis/categories" 
								chipClassname="hidden"
								listItem="text-xs"
							/>
					</CustomAccordion>
					
			  
			  <CustomLink 
			  	name="Notifications" 
			  	linkIcon={<MdNotificationsActive className="h-5 w-5" />} 
			  	linkPath="/ulafis/notifications" 
			  	chipValue="3"
			  />
			</List>
		</>
	)
}

export default NavLinks