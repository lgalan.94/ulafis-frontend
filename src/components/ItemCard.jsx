import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Input,
  Alert,
  Select, 
  Option,
  Textarea
} from "@material-tailwind/react";
import { Link } from 'react-router-dom';
import { CustomButton } from './';
import { useState, useEffect } from 'react';

const ItemCard = ({ 
 _id, 
 name, 
 handleNameChange, 
 description, 
 handleDescriptionChange,
 quantity, 
 handleQuantityChange,
 location,
 handleLocationChange, 
 category,
 categoryLabel,
 handleCategoryChange,
 imageUrl,
 image_url,
 handleImageUrlChange,
 lowStockThreshold,
 handleLowStockThreshold,
 inputNameClass,
 inputQuantityClass,
 inputLowStockThresholdClass,
 inputLocationClass,
 inputCategoryClass,
 inputImageUrlClass,
 inputDescriptionClass,
 divClass,
 textAreaClass,
 displayAlert,
 imageClass
}) => {

const [categories, setCategories] = useState([]);

useEffect(() => {
  fetch(`${import.meta.env.VITE_API_URL}/ulafis/category`)
  .then(response => response.json())
  .then(data => {
     setCategories(data.map(category => {
        return (
            <Option key={category._id} value={category.name} > {category.name.toUpperCase()} </Option>
         )
     }))
  })
}, [])

  return (
    <Card className="flex flex-wrap shadow bg-light">
      <CardBody className="overflow-y-auto max-h-[70vh] scrollbar-thin">

       <div className="flex flex-col flex-wrap mb-3 gap-1">
          {displayAlert}
       </div>
        
        <div className={`${divClass} flex flex-row`}>
           <div className="flex basis-2/5 items-center justify-center">
             <img className={`${imageClass} w-[90%] max-h-[65vh] m-auto shadow-md`} src={imageUrl} />
           </div>

           <div className="basis-3/5 items-center">
             <form className="flex flex-col gap-2">
              
               <div className={inputNameClass}>
                 <Input
                   type="text"
                   size="sm"
                   value={name.toUpperCase()}
                   onChange={handleNameChange}
                   className={`capitalize`}
                   label="Name"
                 />
               </div>
               
               <div className="grid grid-cols-2 gap-x-2">
                <div className={inputQuantityClass}>
                  <Input
                    type="number"
                    size="sm"
                    value={quantity}
                    onChange={handleQuantityChange}
                    className={`capitalize`}
                    label="Quantity"
                  />
                </div>
                <div className={inputLowStockThresholdClass}>
                  <Input
                    type="number"
                    size="sm"
                    value={lowStockThreshold}
                    onChange={handleLowStockThreshold}
                    className={`capitalize`}
                    label="Low Stock Threshold"
                  />
                </div>
               </div>

               <div className="grid grid-cols-2 gap-x-2">
                  
                 <div className={`${inputCategoryClass} w-full`}>
                    <Select onChange={handleCategoryChange} label={categoryLabel}>
                       {categories}
                    </Select>
                 </div>

             
                 <div className={inputLocationClass}>
                   <Input
                     type="text"
                     size="sm"
                     value={location}
                     onChange={handleLocationChange}
                     className={`capitalize`}
                     label="Location"
                   />
                 </div> 
                </div>
  
               <div className={`${inputImageUrlClass} w-full`}>
                 <Textarea 
                   label="Image Url"
                   value={image_url}
                   onChange={handleImageUrlChange}
                   rows={2}
                   resize={true}
                   className="min-h-[40px]"
                 />
               </div>

               <div className={`${inputDescriptionClass} w-full`}>
                 <Textarea 
                   label="Description"
                   value={description} 
                   onChange={handleDescriptionChange}
                   rows={2}
                   resize={true}
                   className="min-h-[40px]"
                 />
               </div>
             </form>
           </div>
        </div>
      </CardBody>
    </Card>
  );
}

export default ItemCard