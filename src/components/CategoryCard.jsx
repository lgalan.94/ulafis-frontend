import {
  Card,
  CardHeader,
  CardBody,
  Typography,
  Button,
  Tooltip
} from "@material-tailwind/react";
import { MdDeleteOutline } from "react-icons/md";
import { FaEdit } from "react-icons/fa";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { CustomDialogBox, CustomButton, ItemCard } from './';
import { useState, useEffect } from 'react';
import { ImSpinner2 } from "react-icons/im";
import { useNavigate } from 'react-router-dom';

const CategoryCard = (props) => {

let navigate = useNavigate()
const { _id, name, image_url } = props.CategoriesProps;
const [open, setOpen] = useState(false);
const [open2, setOpen2] = useState(false);
const [isClicked, setIsClicked] = useState(false);
const [nameAlert, setNameAlert] = useState(false);
const [imageUrlAlert, setImageUrlAlert] = useState(false);
const [isDisabled, setIsDisabled] = useState(true);

const handleOpen = () => setOpen(!open);

const handleDelete = (e) => {
		e.preventDefault();
		setIsClicked(true);
		fetch(`${import.meta.env.VITE_API_URL}/ulafis/category/${_id}`, {
					method: 'DELETE',
					headers: {
							'Content-type': 'application/json'
					}
		})
		.then(response => response.json())
		.then(data => {
					if (data === true) {
								toast.success(`${name.toUpperCase()} Successfully Deleted!`);
								navigate('/ulafis/categories')
					}
					else
					{
							toast.error('Cannot remove item!')
					}
		})
}

const Update = () => {
		navigate(`/ulafis/categories/update/${_id}`);
}

		return (
						<>
								<Card className="w-full group hover:scale-105 max-w-[20rem] bg-blue-gray-50/50 flex-row">
								  <CardHeader
								    className="m-0 w-2/5 shadow-lg shrink-0 rounded-r-none"
								  >
								    <img
								      src={image_url}
								      className="h-full w-full object-cover"
								    />
								  </CardHeader>
								  <CardBody className="h-full flex items-center">
								    <Typography color="gray" className="text-xs font-bold uppercase">
								      {name}
								    </Typography>

		          <Tooltip 
		          		placement="left"
		          		content="Update"
		          		animate={{
		    		        mount: { scale: 1, y: 0 },
		    		        unmount: { scale: 0, y: 25 },
		    		      }}
		    		      className="bg-cyan-600 text-xs"
		          >
		          		<button onClick={Update} className="invisible absolute p-1 hover:scale-105 hover:shadow-lg hover:z-10 top-2 right-2 group-hover:visible bg-gray-300 rounded-full text-cyan-600"> 
		          		    <FaEdit className="h-5 w-5" /> 
		          		</button>
		          </Tooltip>

		          <Tooltip 
		          		placement="left"
		          		content="Delete"
		          		animate={{
		    		        mount: { scale: 1, y: 0 },
		    		        unmount: { scale: 0, y: 25 },
		    		      }}
		    		      className="bg-red-600 text-xs"
		          >
		          		<button onClick={handleOpen} className="invisible absolute p-1 hover:scale-105 hover:shadow-lg bottom-2 right-2 group-hover:visible bg-gray-300 rounded-full text-red-600"> 
		          		    <MdDeleteOutline className="h-5 w-5" /> 
		          		</button>
		          </Tooltip> 
								  </CardBody>
								</Card>

								<CustomDialogBox
										size="xs"
										open={open}
										handleOpen={handleOpen}
										title="Confirmation"
										closeButton={handleOpen}
								>
										<div>
										  <Typography>Delete {name.toUpperCase()}?</Typography>
										  <div className="mt-8 flex flex-row gap-1 justify-center mx-auto">
										      <CustomButton
										        label="Cancel"
										        handleClick={handleOpen}
										        btnClass="rounded px-3 py-2 hover:scale-105"
										      />

										      {
										        !isClicked ? (
										           <CustomButton
										             label="Delete"
										             handleClick={handleDelete}
										             btnClass="rounded px-3 py-2 hover:scale-105"
										             color="red"
										           />
										         ) : (
										           <CustomButton
										             label="Deleting..."
										             btnClass="rounded px-3 py-2 hover:scale-105"
										             color="red"
										             icon={<ImSpinner2 className="animate-spin" />}
										           />
										         )
										      }

										  </div>
										</div>
								</CustomDialogBox>

								

								<div className="absolute">
								  <ToastContainer position="top-right" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
								</div>	
						</>
			)
}

export default CategoryCard;

