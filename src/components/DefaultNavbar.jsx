import React from 'react'
import {
  Navbar,
  Collapse,
  Typography,
  Button,
  IconButton,
  Card,
} from "@material-tailwind/react";
import {
  Link,
  useLocation,
  useNavigate
} from 'react-router-dom';

import { useState, useEffect } from 'react';
import { IoMdNotifications } from "react-icons/io";
import { CustomButton, NotificationsMenu, ProfileMenu } from './';
 
const DefaultNavbar = ({ userRole, navClass }) => {
  const navigate = useNavigate();
  const [openNav, setOpenNav] = useState(false);

  let [shortName, setShortName] = useState('');

  useEffect(() => {
      fetch(`${import.meta.env.VITE_API_URL}/ulafis/settings`)
        .then((result) => result.json())
        .then((data) => {
          const sName = data.find((setting) => setting.key === 'SHORT NAME');
          if (sName) {
            setShortName(sName.value);
          } else {
              setShortName('')
          }
        });
    }, [shortName]);

  useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 960 && setOpenNav(false),
    );
  }, []);

 

  return (
    <>
    <div className="sticky top-0 z-10 max-h-[768px] w-full ">
      <Navbar className={`${navClass} z-10 h-max max-w-full rounded-none px-4 py-1 lg:px-8 shadow`}>
        <div className="flex items-center justify-between text-blue-gray-900">
          <Typography
            className="mr-4 cursor-pointer py-1.5 font-semibold uppercase"
          >
            <span className="italic"> </span><span className="font-md uppercase text-orange-500">&copy; {shortName}</span> <span>&nbsp;{userRole}</span> <span className="lowercase italic">&nbsp;</span>
          </Typography>
          <div className="flex items-center gap-4">
              {/*{
                user.id !== null ? (
                     <Button
                       variant="text"
                       size="sm"
                       className="hidden lg:inline-block"
                       onClick={() => navigate('/ulafis/logout')}
                     >
                       <span>Logout</span>
                     </Button>
                 ) : (
                   <Button
                     variant="text"
                     size="sm"
                     className="hidden lg:inline-block"
                     onClick={() => navigate('/ulafis/login')}
                   >
                     <span>Login</span>
                   </Button>
                 )
              }*/}

              <div className="flex flex-col hover:scale-105">
                <span className="absolute inline-flex items-center rounded-full bg-red-500 px-[4px] py-[1px] text-white text-[8px] font-bold ml-5 z-10 mt-1">
                  1
                </span>
                <NotificationsMenu />
              </div>

              <ProfileMenu />

              <CustomButton
                label="login"
                variant="text"
                handleClick={() => navigate('/ulafis/login')}
              />
          </div>
        </div>
      </Navbar>
    </div>

    
    </>

  );
}

export default DefaultNavbar