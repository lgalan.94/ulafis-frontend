import { List, ListItem, ListItemPrefix, ListItemSuffix, Typography, Chip, Accordion, AccordionHeader, AccordionBody } from '@material-tailwind/react';
import { NavLink, useLocation } from 'react-router-dom'
import { TbCategoryPlus } from "react-icons/tb";
import { FiHome } from "react-icons/fi";
import { IoMdAdd } from "react-icons/io";
import { MdNotificationsActive } from "react-icons/md";
import { MdOutlineInventory } from "react-icons/md";
import { useState, useEffect } from 'react';
import { SiManageiq } from "react-icons/si";
import { FaUsersGear } from "react-icons/fa6";
import { GoReport } from "react-icons/go";
import { IoIosSettings } from "react-icons/io";

import {
  PresentationChartBarIcon,
  ShoppingBagIcon,
  UserCircleIcon,
  Cog6ToothIcon,
  InboxIcon,
  PowerIcon,
} from "@heroicons/react/24/solid";
import { ChevronRightIcon, ChevronDownIcon } from "@heroicons/react/24/outline";

const AdminNavLinks = () => {

	let currentPath = useLocation().pathname;
	let settingsPath = '/ulafis/admin/settings';
	let addSettingsPath = '/ulafis/admin/settings/add';
	let usersPath = '/ulafis/admin/manage-users';

	const [open, setOpen] = useState(0); 
 const handleOpen = (value) => {
   setOpen(open === value ? 0 : value);
 };

	const CustomLink = ({ name, linkIcon, linkPath, className, chipValue, chipClassname, listItem  }) => {

	  return (
	    <NavLink 
	      as={NavLink}
	      to={linkPath}
	      className={`${className} ${currentPath === linkPath ?  'bg-light shadow-sm rounded-full' : 'bg-none' } `}
	    >
	      <ListItem className={`py-1 hover:shadow-sm hover:rounded-full hover:scale-105 ${listItem}`}>
	        <ListItemPrefix>
	          {linkIcon}
	        </ListItemPrefix>
	        {name}
	        <ListItemSuffix>
            <span className={`${chipClassname} text-xs bg-gray-100 px-2 py-1 rounded-full`}>{chipValue}</span>
          </ListItemSuffix>
	      </ListItem>
	    </NavLink>
	  )
	}

	const CustomAccordion = ({ children, title, icon, a_value }) => {
				return (
						<Accordion
			     open={open === a_value}
			     icon={
			       <ChevronDownIcon
			         strokeWidth={2.5}
			         className={`mx-auto h-4 w-4 transition-transform ${open === a_value ? "rotate-180" : ""}`}
			       />
			     }
			   >
			     <ListItem className="p-0" selected={open === a_value}>
			       <AccordionHeader onClick={() => handleOpen(a_value)} className="border-b-0 px-3 py-1">
			         <ListItemPrefix>
			           {icon}
			         </ListItemPrefix>
			         <Typography color="blue-gray" className="mr-auto font-normal">
			           {title}
			         </Typography>
			       </AccordionHeader>
			     </ListItem>
			     <AccordionBody className="py-1">
			       <List className="ml-3 p-0">
			         	{children}
			       </List>
			     </AccordionBody>
			   </Accordion>
				)
	}

	return (
		<>
			<List className="">
					<CustomLink 
						name="Dashboard" 
						linkIcon={<PresentationChartBarIcon className="h-5 w-5" />} 
						linkPath="/ulafis/admin" 
						chipClassname="hidden"
					/>
					<CustomAccordion
							title="Users"
							icon={<FaUsersGear className="h-5 w-5" />}
							a_value={
										currentPath === usersPath ? 0 : 1
							}
					>	
							<CustomLink 
								name="Manage Users" 
								linkIcon={<ChevronRightIcon strokeWidth={3} className="h-3 w-5" />} 
								linkPath="/ulafis/admin/manage-users" 
								chipClassname="hidden"
								listItem="text-xs"
							/>
							<CustomLink 
								name="Add User" 
								linkIcon={<IoMdAdd strokeWidth={3} className="h-3 w-5" />} 
								linkPath="/ulafis/inventory/add-item" 
								chipClassname="hidden"
								listItem="text-xs"
							/>
					</CustomAccordion>
					<CustomLink 
						name="Reports" 
						linkIcon={<GoReport className="h-5 w-5" />} 
						linkPath="/ulafis/admin/reports" 
						chipClassname="hidden"
					/>
					<CustomAccordion
							title="Settings"
							icon={<IoIosSettings className="h-5 w-5" />}
							a_value={
										currentPath === addSettingsPath || currentPath === settingsPath ? 0 : 2
							}
					>	
							<CustomLink 
								name="Manage Settings" 
								linkIcon={<ChevronRightIcon strokeWidth={3} className="h-3 w-5" />} 
								linkPath="/ulafis/admin/settings" 
								chipClassname="hidden"
								listItem="text-xs"
							/>
							<CustomLink 
								name="Add New" 
								linkIcon={<IoMdAdd strokeWidth={3} className="h-3 w-5" />} 
								linkPath="/ulafis/admin/settings/add" 
								chipClassname="hidden"
								listItem="text-xs"
							/>
					</CustomAccordion>
			</List>
		</>
	)
}

export default AdminNavLinks