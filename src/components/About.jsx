
const About = ({ title, image, subtitle, description }) => {
	return (
			<div className="text-center py-10">
				<div className="text-5xl font-semibold uppercase p-5">{title}</div>
				<img className="mx-auto" src={image} />

				<div className="font-semibold p-5">{subtitle}</div>

				<div className="w-[55%] mx-auto text-justify">{description}</div>

			</div>
	)
}

export default About