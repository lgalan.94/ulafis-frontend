import { Link } from 'react-router-dom';
import { Typography, Button, Card, CardBody, CardFooter} from '@material-tailwind/react';
import { CustomDialogBox, InvisibleButton } from './';
import { useState, useEffect } from 'react';
import { FaRegEdit } from "react-icons/fa";
import { AiFillDelete } from "react-icons/ai";
import { useNavigate } from 'react-router-dom';
import { CustomButton } from './';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ImSpinner2 } from "react-icons/im";
import { MdDeleteOutline } from "react-icons/md";
import { IoMdEye } from "react-icons/io";

const ItemsList = (props) => {

  const { _id, name, description, image_url, category } = props.itemProps;
  const [open, setOpen] = useState(false); 
  const handleOpen = () => setOpen(!open);
  const [open2, setOpen2] = useState(false); 
  const handleOpen2 = () => {
    setOpen2(!open2)
    setOpen(false);
  }
  const [isClicked, setIsClicked] = useState(false);

  const [itemName, setItemName] = useState('');
  const [itemDescription, setItemDescription] = useState('');
  const [itemQuantity, setItemQuantity] = useState(0);
  const [itemThreshold, setItemThreshold] = useState(0);
  const [itemLocation, setItemLocation] = useState('');
  const [itemCategory, setItemCategory] = useState('');
  const [itemImageUrl, setItemImageUrl] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`${import.meta.env.VITE_API_URL}/ulafis/items/${_id}`, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(data => {
        setItemName(data.name);
        setItemDescription(data.description);
        setItemQuantity(data.quantity);
        setItemLocation(data.location);
        setItemCategory(data.category);
        setItemImageUrl(data.image_url);
        setItemThreshold(data.low_stock_threshold);
        setLoading(false);
    })
  }, [_id])

  const navigate = useNavigate();
  const handleUpdate = () => {
    navigate(`/ulafis/update/${_id}`)
  }

  const handleDelete = (e) => {
     e.preventDefault();
     setIsClicked(true);
     fetch(`${import.meta.env.VITE_API_URL}/ulafis/items/${_id}`, {
       method: "DELETE",
       headers: {
         'Content-type': 'application/json'
       }
     })
     .then(result => result.json())
     .then(response => {
       if (response === true) {
          toast.success(`${name} Successfully Deleted!`);
          setOpen2(false);
          setIsClicked(false);
       } else {
         toast.error('Cannot delete item')
         setOpen2(false);
         setIsClicked(false);
       }
     })
  }
  
  return (
    <>
      <tr className="hover:bg-blue-gray-50/50 group hover:shadow-lg text-xs">
        <td className="border-b border-blue-gray-50">
          <img className="w-9 border border-dark/10 ml-1 shadow shadow-sm h-9 rounded-full" src={image_url} />
        </td>
        <td className="border-b border-blue-gray-50">
          <Typography className="text-md" >{name.toUpperCase().substring(0, 25) + ""}</Typography>
        </td>
        <td className="border-b border-blue-gray-50">
          {category}
        </td>
        <td className="border-b border-blue-gray-50">
          {description.substring(0, 50) + "..."}
        </td>
        <td className="border-b border-blue-gray-50">
          <div className="flex flex-row">
            <InvisibleButton 
              handleClick={handleOpen}
              label="view"
              icon={<IoMdEye className="w-4 h-4" />}
            />
            <InvisibleButton 
              handleClick={handleUpdate}
              label="edit"
              icon={<FaRegEdit className="w-4 h-4" />}
              tooltip="!bg-green-600"
              buttonClass="!text-green-600"
            />
            <InvisibleButton 
              handleClick={handleOpen2}
              label="delete"
              icon={<MdDeleteOutline className="w-4 h-4" />}
              tooltip="!bg-red-600"
              buttonClass="!text-red-600"
            />
          </div>
        </td>
      </tr>

      <CustomDialogBox
        size="sm"
        title="Details"
        open={open} 
        handleOpen={handleOpen}
        closeButton={handleOpen}
      >

       <Card className="flex shadow bg-light">
         {
           !loading ? (
              <CardBody className="flex flex-row"> 
               <div className="flex basis-2/5">
                 <img src={itemImageUrl} className="border w-[90%] max-h-[65vh] m-auto border-2 rounded-md" />
               </div>
               <div className="flex gap-1 flex-col items-start justify-center basis-3/5 px-3">
                 <Typography variant="h6" className="font-bold text-orange-500">
                   {itemName.toUpperCase()}
                 </Typography>
                 <div className="flex items-start flex-col py-2">
                     <Typography className="italic text-xs text-dark/75">Description:</Typography>
                     <Typography className="text-sm flex text-start leading-3 text-orange-300">{itemDescription}</Typography>
                 </div>
                 <Typography className="text-xs text-dark/75">
                   <span className="italic">Quantity: &nbsp;</span> <span className="text-orange-300 text-sm">{itemQuantity}</span>
                 </Typography>
                 <Typography className="text-xs text-dark/75">
                   <span className="italic">Low Stock Threshold: &nbsp;</span> <span className="text-orange-300 text-sm">{itemThreshold}</span>
                 </Typography>
                 <Typography className="text-xs text-dark/75">
                   <span className="italic">Location: &nbsp;</span> <span className="text-orange-300 text-sm">{itemLocation}</span>
                 </Typography>
                 <Typography className="text-xs text-dark/75">
                   <span className="italic">Category: &nbsp;</span> <span className="text-orange-300 text-sm">{itemCategory}</span>
                 </Typography>
               </div>
              </CardBody>
           ) : (
             <ImSpinner2 className="w-12 h-12 min-h-[50vh] mx-auto animate-spin" />
           )
         }
         <CardFooter className="pt-0 flex flex-row gap-1 mx-auto">
             <CustomButton
                label="update"
                handleClick={handleUpdate}
                icon={<FaRegEdit className="h-2.5 w-2.5" />}
                btnClass="capitalize hover:scale-105"
             />
             <CustomButton
                label="delete"
                handleClick={handleOpen2}
                icon={<AiFillDelete className="h-2.5 w-2.5" />}
                color="red"
                btnClass="capitalize hover:scale-105"
             />
         </CardFooter>
       </Card>

      </CustomDialogBox>

      <CustomDialogBox
        buttonClass="hidden"
        size="xs"
        title="Confirmation"
        open={open2} 
        handleOpen={handleOpen2}
      >

      <div>

        <Typography>Delete {name}?</Typography>

        <div className="mt-8 flex flex-row gap-1 justify-center mx-auto">

            

            <CustomButton
              label="Cancel"
              handleClick={handleOpen2}
              btnClass="rounded px-3 py-2 hover:scale-105"
            />

            {
              !isClicked ? (
                 <CustomButton
                   label="Delete"
                   handleClick={handleDelete}
                   btnClass="rounded px-3 py-2 hover:scale-105"
                   color="red"
                 />
               ) : (
                 <CustomButton
                   label="Deleting..."
                   btnClass="rounded px-3 py-2 hover:scale-105"
                   color="red"
                   icon={<ImSpinner2 className="animate-spin" />}
                 />
               )
            }

        </div>
      </div>

      </CustomDialogBox>

      <div className="absolute">
        <ToastContainer position="top-right" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
      </div>
    </>
  );
};

export default ItemsList;
