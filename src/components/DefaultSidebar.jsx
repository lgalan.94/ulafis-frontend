import {
  Card,
  Typography,
  List,
  ListItem,
  ListItemPrefix,
  ListItemSuffix,
  Chip,
  Drawer,
  Button,
  IconButton
} from "@material-tailwind/react";
import NavLinks from './NavLinks';

import { ChevronRightIcon, ChevronDownIcon } from "@heroicons/react/24/outline";
import { TbGridDots } from "react-icons/tb";
 
import {
  Link,
  useLocation,
  useNavigate
} from 'react-router-dom';

import React,  { useState, useEffect } from 'react'

 
export default function DefaultSidebar({ sidebarClass }) {

  const [openNav, setOpenNav] = useState(false);
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(0);
  
  useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 960 && setOpenNav(false),
    );
  }, []);

 
  return (
  <>
    <Card floated={false} shadow={false} className={`${sidebarClass} h-full min-h-[91vh] max-h-[91vh] w-full max-w-[18rem] p-1 bg-defaultColor`}>
      <div className="mb-2 capitalize p-4">
        <Typography variant="h5" color="blue-gray">
          <NavLinks />
        </Typography>
      </div>
    </Card>
    

  </>

      
  );
}