const LabData = [ 
    {
      "id": 1,
      "name": "Pipettes, 1000uL",
      "description": "Sterile, disposable pipettes.",
      "quantity": 200,
      "location": "Lab room 1, shelf B",
      "category": "Consumables",
      "low_stock_threshold": 50,
      "image_url": "sample.jpg"
    },
    {
      "id": 2,
      "name": "Microscope slides",
      "description": "Plain glass slides for microscopy.",
      "quantity": 1000,
      "location": "Lab room 2, drawer A",
      "category": "Consumables",
      "low_stock_threshold": 200,
      "image_url": "sample.jpg"
    },
    {
      "id": 3,
      "name": "Centrifuge tubes, 15mL",
      "description": "Sterile, disposable centrifuge tubes.",
      "quantity": 500,
      "location": "Lab room 1, refrigerator",
      "category": "Consumables",
      "low_stock_threshold": 100,
      "image_url": "sample.jpg"
    },
    {
      "id": 4,
      "name": "Spectrophotometer",
      "description": "UV-Vis spectrophotometer for measuring absorbance.",
      "quantity": 1,
      "location": "Lab room 3",
      "category": "Equipment",
      "low_stock_threshold": 0,
      "image_url": "sample.jpg"
    },
    {
      "id": 5,
      "name": "Micropipette",
      "description": "Electronic micropipette for accurate volume measurements.",
      "quantity": 2,
      "location": "Lab room 2",
      "category": "Equipment",
      "low_stock_threshold": 1,
      "image_url": "sample.jpg"
    },
    {
      "id": 6,
      "name": "Pipettes, 1000uL",
      "description": "Sterile, disposable pipettes.",
      "quantity": 200,
      "location": "Lab room 1, shelf B",
      "category": "Consumables",
      "low_stock_threshold": 50,
      "image_url": "sample.jpg"
    },
    {
      "id": 7,
      "name": "Microscope slides",
      "description": "Plain glass slides for microscopy.",
      "quantity": 1000,
      "location": "Lab room 2, drawer A",
      "category": "Consumables",
      "low_stock_threshold": 200,
      "image_url": "sample.jpg"
    },
    {
      "id": 8,
      "name": "Centrifuge tubes, 15mL",
      "description": "Sterile, disposable centrifuge tubes.",
      "quantity": 500,
      "location": "Lab room 1, refrigerator",
      "category": "Consumables",
      "low_stock_threshold": 100,
      "image_url": ""
    },
    {
      "id": 9,
      "name": "Spectrophotometer",
      "description": "UV-Vis spectrophotometer for measuring absorbance.",
      "quantity": 1,
      "location": "Lab room 3",
      "category": "Equipment",
      "low_stock_threshold": 0,
      "image_url": "sample.jpg"
    },
    {
      "id": 10,
      "name": "Micropipette",
      "description": "Electronic micropipette for accurate volume measurements.",
      "quantity": 2,
      "location": "Lab room 2",
      "category": "Equipment",
      "low_stock_threshold": 1,
      "image_url": "sample.jpg"
    }
  ]

export default LabData